//
//  ShoppingItemCell.swift
//  Lista_zakupów
//
//  Created by Bartosz Kucharski on 15.12.2016.
//  Copyright © 2016 Bartosz Kucharski. All rights reserved.
//

import UIKit

class ShoppingItemCell: UITableViewCell {

  @IBOutlet weak var itemNameLabel: UILabel!
  @IBOutlet weak var itemDateLabel: UILabel!
  @IBOutlet weak var itemImageView: UIImageView!
  
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  

}
