//
//  NewPositionViewController.swift
//  Lista_zakupów
//
//  Created by Bartosz Kucharski on 15.12.2016.
//  Copyright © 2016 Bartosz Kucharski. All rights reserved.
//

import UIKit

protocol DataSentDelegate {
  func userDidEnterDataOk (data: String, date: Date)
  func userDidCancel ()
}

class NewPositionViewController: UIViewController {
  
  var delegate: DataSentDelegate? 
  
  @IBOutlet weak var dataTextField: UITextField!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  
  
  @IBAction func okBtnWasPressed(_ sender: UIButton) {
    if dataTextField.text != nil {
      let data = dataTextField.text
      let date = Date()
      delegate?.userDidEnterDataOk(data: (data!), date: date )
    }
  }
  
  
  @IBAction func cancelBtnWasPressed(_ sender: UIButton) {
    delegate?.userDidCancel()
    
  }
  
  
}
